#!/usr/bin/env python3

import toml
import pathlib
from datetime import datetime

_autouploadFiles = []

class AutoUpload(object):
    """AutoUpload is a class handling the state of videos automatically uploaded by prismedia"""

    _videoSuffix = ".mp4"

    urlSuffix = "-url"
    errorSuffix = "-error"
    publishSuffix = "-publish"
    lastUpdateTimeKey = "update-time"

    def __init__(self, autouploadFile):
        super(AutoUpload, self).__init__()

        # print("content of file")
        # with open(autouploadFile, "r") as file:
        #     print(file.readlines())

        self._baseAutouploadFile = autouploadFile
        self._basePath = pathlib.Path(autouploadFile).resolve().parent
        self._load()

    def nextVideo(self, platform, recursive=True):
        """Get the path to the next video to upload for a specific platform"""

        # Get next video from current autoupload file
        for nextVideo in self._autoUploadConfig["videos"]:
            if platform + AutoUpload.urlSuffix not in self._autoUploadConfig["videos"][nextVideo]:
                videoPath = (self._basePath / nextVideo).with_suffix(self._videoSuffix).resolve().as_posix()
                return Video(self, platform, nextVideo, videoPath, (self._baseAutouploadFile, self._autoUploadConfig))

        # Get next video from another autoupload file
        if recursive:
            for file in _autouploadFiles:
                if file[0] != self._baseAutouploadFile:
                    nextVideo = file[1].nextVideo(platform, False)
                    if nextVideo:
                        videoPath = (self._basePath / nextVideo.videoName).with_suffix(self._videoSuffix).resolve().as_posix()
                        return Video(self, platform, nextVideo.videoName, nextVideo.videoPath, (file[0], file[1]._autoUploadConfig))

        # No video to upload for this platforms
        return None

    def reload(self):
        """Reload the configuration"""

        _autouploadFiles.clear()
        self._load()

    def _load(self):
        """Private helper function
        Load the configuration file."""
        # We can reuse this function to reload the configuration after saving the files, or not

        self._autoUploadConfig = toml.load(self._baseAutouploadFile)
        _autouploadFiles.append((self._baseAutouploadFile, self))

        # This should take care of circular references
        # TODO: make a test for this
        for autouploadFile in self._autoUploadConfig.get("autoupload", []):
            alreadyPresentAutoupload = False

            for file in _autouploadFiles:
                if file[0] == autouploadFile:
                    alreadyPresentAutoupload = True

            if not alreadyPresentAutoupload:
                AutoUpload(autouploadFile)

        # TODO: remove me / should only be here for debug
        # print("setting autouplad config")
        # print(self._baseAutouploadFile)
        # print(self._autoUploadConfig)


class Video(object):
    """Returned by AutoUpload when looking for the next Video to upload."""

    def __init__(self, parent, platform, videoName, videoPath, autouploadFile):
        super(Video, self).__init__()
        self._autouploadFile = autouploadFile

        self.parent = parent
        self.platform = platform
        self.videoName = videoName
        self.videoPath = videoPath

    def success(self, url, publishDate):
        """Last video asked was successfully uploaded"""

        updateTime = datetime.today()

        # Remove last error if there were any
        self._autouploadFile[1]["videos"][self.videoName].pop(self.platform + AutoUpload.errorSuffix, None)

        self._autouploadFile[1]["videos"][self.videoName][AutoUpload.lastUpdateTimeKey] = updateTime
        self._autouploadFile[1]["videos"][self.videoName][self.platform + AutoUpload.urlSuffix] = url
        self._autouploadFile[1]["videos"][self.videoName][self.platform + AutoUpload.publishSuffix] = publishDate

        self._write()

    def error(self, errorString):
        """There was an error on upload of last video"""

        updateTime = datetime.today()

        self._autouploadFile[1]["videos"][self.videoName][AutoUpload.lastUpdateTimeKey] = updateTime
        self._autouploadFile[1]["videos"][self.videoName][self.platform + AutoUpload.errorSuffix] = errorString

        self._write()

    def _write(self):
        """Private helper function
        Write current autoupload state on file(s)"""

        with open(self._autouploadFile[0], "w", encoding="utf-8", errors="strict") as f:
            toml.dump(self._autouploadFile[1], f, encoder=toml.TomlPreserveInlineDictEncoder())  # can we also inherit from toml.TomlPreserveCommentEncoder()?

        self.parent.reload()
