# Prismedia AutoUpload
This the implementation for the file handling of prismedia's autoupload feature.

It consist of one file: `autoupload.py`. There is also a test file as `autoupload_test.py` which can be helpfull has to how to use this object.

I am not a python expert and everything may change when integrating this project in Prismedia: function/variable name/case, interface, ...

# Dependencies
```sh
pip3 install toml
```

# Tests
Since there is no tests done for Prismedia, I choose `unittest` to do tests for this lib since it is present in the python standard library.

Launch tests with
```sh
python3 -m unittest
```

# git hook
To start tests automatically you can create a `pre-commit` file in `.git/hooks/` with the following content:
```sh
#!/usr/bin/env bash

# run tests
python3 -m unittest discover . --failfast
# unittest exists with 0 if all tests passed, 1 otherwise.
tests_failed=$?
if [ ${tests_failed} -eq 1 ]; then
	echo "Tests failed. Aborting commit"
	exit 1
fi

exit 0
```
The hook should have execution right, `chmod 775 .git/hooks/pre-commit`.

Maybe we can use the example here https://gist.github.com/orenshk/6cab23fcff847d704af7f3eec22b1ba6 as pre-commit hook to also format according to PEP8.
