#!/usr/bin/env python3

import autoupload
from datetime import datetime
import os
import tempfile
import unittest

class TestVideoMethods(unittest.TestCase):

    platform1 = "platform"
    platform2 = "otherplatform"

    def tearDown(self):
        # Reset the kinda static variable between runs.
        autoupload._autouploadFiles = []

    def test_nextVideo(self):
        with TestFileContent(
"""[videos]
Episode1 = {}
"""
        ) as videoFile:
            auto = autoupload.AutoUpload(videoFile.filename)

            nextVideo = auto.nextVideo(self.platform1)
            self.assertEqual(nextVideo.videoPath, "/tmp/Episode1.mp4")
            nextVideo = auto.nextVideo(self.platform2)
            self.assertEqual(nextVideo.videoPath, "/tmp/Episode1.mp4")

    def test_nextVideoSubfolder(self):
        with TestFileContent(
"""[videos]
"subfolder/Episode1" = {}
"""
        ) as videoFile:
            auto = autoupload.AutoUpload(videoFile.filename)

            nextVideo = auto.nextVideo(self.platform1)
            self.assertEqual(nextVideo.videoPath, "/tmp/subfolder/Episode1.mp4")
            nextVideo = auto.nextVideo(self.platform2)
            self.assertEqual(nextVideo.videoPath, "/tmp/subfolder/Episode1.mp4")

    def test_success(self):
        with TestFileContent(
"""[videos]
Episode1 = {}
"""
        ) as videoFile:
            auto = autoupload.AutoUpload(videoFile.filename)

            nextVideo = auto.nextVideo(self.platform1)
            nextVideo.success("https://platform/url", datetime(2020, 8, 28, 17, 54, 31))

            nextVideo = auto.nextVideo(self.platform1)
            self.assertEqual(nextVideo, None)
            nextVideo = auto.nextVideo(self.platform2)
            self.assertEqual(nextVideo.videoPath, "/tmp/Episode1.mp4")
            # TODO: read file and check formatting and content?

            # print("content of file")
            # with open(videoFile.filename, "r") as file:
            #     print(file.readlines())

    def test_error(self):
        with TestFileContent(
"""[videos]
Episode1 = {}
"""
        ) as videoFile:
            auto = autoupload.AutoUpload(videoFile.filename)

            nextVideo = auto.nextVideo(self.platform1)
            nextVideo.error("Server not available")

            nextVideo = auto.nextVideo(self.platform1)
            self.assertEqual(nextVideo.videoPath, "/tmp/Episode1.mp4")
            # TODO: read file and check formatting and content?

            # print("content of file")
            # with open(videoFile.filename, "r") as file:
            #     print(file.readlines())

    def test_errorThenSuccess(self):
        with TestFileContent(
"""[videos]
Episode1 = {}
"""
        ) as videoFile:
            auto = autoupload.AutoUpload(videoFile.filename)

            nextVideo = auto.nextVideo(self.platform1)
            nextVideo.error("Server not available")

            nextVideo = auto.nextVideo(self.platform1)
            # TODO: read file and check formatting and content?

            # print("content of file")
            # with open(videoFile.filename, "r") as file:
            #     print(file.readlines())

            nextVideo.success("https://platform/url", datetime(2020, 8, 28, 17, 54, 31))
            nextVideo = auto.nextVideo(self.platform1)
            self.assertEqual(nextVideo, None)
            nextVideo = auto.nextVideo(self.platform2)
            self.assertEqual(nextVideo.videoPath, "/tmp/Episode1.mp4")
            # TODO: read file and check formatting and content?
            # TODO: check the key "platform-error" is not present after a success

            # print("content of file")
            # with open(videoFile.filename, "r") as file:
            #     print(file.readlines())

    def test_subAutouploadFile(self):
        # We may split this test to first test that we can have reference another file
        # and then that we pass transparently between files.
        with TestFileContent(
"""[videos]
Episode2 = {}   # TODO: We should chek that the path of this file is from the base path of this autoupload config file
"""
        ) as autouploadFile:
            with TestFileContent(
f"""autoupload = ["{autouploadFile.filename}"] # list of strings, one for each sub autoupload file

[videos]
Episode1 = {{}} # Double curly braces to escape them for f-string
"""
            ) as videoFile:
                auto = autoupload.AutoUpload(videoFile.filename)

                nextVideo = auto.nextVideo(self.platform1)
                nextVideo.success("https://platform/url", datetime(2020, 8, 28, 17, 54, 31))

                nextVideo = auto.nextVideo(self.platform1)
                self.assertEqual(nextVideo.videoPath, "/tmp/Episode2.mp4")
                # TODO: read file and check formatting and content?
                nextVideo.success("https://platform/url", datetime(2020, 8, 28, 17, 54, 31))

                nextVideo = auto.nextVideo(self.platform1)
                self.assertEqual(nextVideo, None)
                nextVideo = auto.nextVideo(self.platform2)
                self.assertEqual(nextVideo.videoPath, "/tmp/Episode1.mp4")
                # TODO: read file and check formatting and content?

                # print("content of autoupload file")
                # with open(autouploadFile.filename, "r") as file:
                #     print(file.readlines())
                #
                # print("content of video file")
                # with open(videoFile.filename, "r") as file:
                #     print(file.readlines())

# https://stackoverflow.com/a/54053967
class TestFileContent:
    def __init__(self, content):

        self.file = tempfile.NamedTemporaryFile(mode="w", delete=False)

        with self.file as f:
            f.write(content)

    @property
    def filename(self):
        return self.file.name

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        os.unlink(self.filename)

if __name__ == '__main__':
    unittest.main()
